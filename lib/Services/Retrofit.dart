import 'dart:core';
import 'package:devaseva_app/models/loginResponse_Model.dart';
import 'package:devaseva_app/models/loginrequest_model.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';



part 'Retrofit.g.dart';

@RestApi(baseUrl: 'https://testitecywebapi.azurewebsites.net/Jobseeker/')




abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;


  @GET("Login")
  Future<LoginResponse> login(@Body() LoginRequest loginRequest);






}


