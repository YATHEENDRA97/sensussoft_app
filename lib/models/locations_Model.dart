// To parse this JSON data, do
//
//     final locationsListResponse = locationsListResponseFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

LocationsListResponse locationsListResponseFromJson(String str) => LocationsListResponse.fromJson(json.decode(str));

String locationsListResponseToJson(LocationsListResponse data) => json.encode(data.toJson());

class LocationsListResponse {
  LocationsListResponse({
    this.id,
    this.title,
    this.subtitle,
    this.time,
    this.cost,
    this.distance,
    this.image,
    this.subimage,
  });

  String? id;
  String? title;
  String? subtitle;
  String? time;
  String? cost;
  String? distance;
  String? image;
  String? subimage;

  factory LocationsListResponse.fromJson(Map<String, dynamic> json) => LocationsListResponse(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    subtitle: json["subtitle"] == null ? null : json["subtitle"],
    time: json["time"] == null ? null : json["time"],
    cost: json["cost"] == null ? null : json["cost"],
    distance: json["distance"] == null ? null : json["distance"],
    image: json["image"] == null ? null : json["image"],
    subimage: json["subimage"] == null ? null : json["subimage"],


  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "subtitle": subtitle == null ? null : subtitle,
    "time": time == null ? null : time,
    "cost": cost == null ? null : cost,
    "distance": distance == null ? null : distance,
    "image": image == null ? null : image,
    "subimage": subimage == null ? null : subimage,


  };
}
