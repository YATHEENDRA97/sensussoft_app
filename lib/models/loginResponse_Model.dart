// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.message,
    this.didError,
    this.errorMessage,
  });

  String? message;
  bool? didError;
  String? errorMessage;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    message: json["message"] == null ? null : json["message"],
    didError: json["didError"] == null ? null : json["didError"],
    errorMessage: json["errorMessage"] == null ? null : json["errorMessage"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "didError": didError == null ? null : didError,
    "errorMessage": errorMessage == null ? null : errorMessage,
  };
}
