// To parse this JSON data, do
//
//     final loginRequest = loginRequestFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));

String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
  String? userName;
  String? password;

  LoginRequest({
    this.userName,
    this.password,
  });

  factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
    userName: json["userName"] == null ? null : json["userName"],
    password: json["password"] == null ? null : json["password"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName == null ? null : userName,
    "password": password == null ? null : password,
  };
}
