// To parse this JSON data, do
//
//     final toDoModel = toDoModelFromJson(jsonString);

// ignore_for_file: prefer_if_null_operators

import 'dart:convert';

ToDoModel toDoModelFromJson(String str) => ToDoModel.fromJson(json.decode(str));

String toDoModelToJson(ToDoModel data) => json.encode(data.toJson());

class ToDoModel {
  ToDoModel({
    this.id,
    this.title,
    this.description,
    this.status,
    this.date,
  });

  int? id;
  String? title;
  String? description;
  String? status;
  String? date;

  factory ToDoModel.fromJson(Map<String, dynamic> json) => ToDoModel(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    status: json["status"] == null ? null : json["status"],
    date: json["date"] == null ? null : json["date"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "status": status == null ? null : status,
    "date": date == null ? null : date,
  };
}
