// ignore_for_file: prefer_const_constructors, prefer_final_fields

import 'package:devaseva_app/models/to_do_model.dart';
import 'package:devaseva_app/utils/db_helper.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PostToDoItem extends StatefulWidget {
  ToDoModel toDoModel;
  String appBarTitle;

  PostToDoItem(this.toDoModel, this.appBarTitle);
  @override
  _PostToDoItemState createState() =>
      _PostToDoItemState(this.toDoModel, this.appBarTitle);
}

class _PostToDoItemState extends State<PostToDoItem> {
  ToDoModel toDoModel;
  String appBarTitle;

  List<String> _statusesList = ["Pending", "Completed"];

  String selectedStatus = "Pending";

  String _chosenValue = "Merchant";


  TextEditingController _titleEditingController = TextEditingController();
  TextEditingController _descriptionEditingController = TextEditingController();

  _PostToDoItemState(this.toDoModel, this.appBarTitle);

  @override
  void initState() {
    selectedStatus = (toDoModel.status?.length == 0 ? "Pending" : toDoModel.status??"");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // _titleEditingController.text = toDoModel.title!??"";
    // _descriptionEditingController.text = toDoModel.description!??"";

    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      body: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            // DropdownButton<String>(
            //     value: selectedStatus,
            //     items: <String>["Pending", "Completed"].map<DropdownMenuItem<String>>((String value) {
            //       return DropdownMenuItem<String>(child: Text(value), value: value);
            //     }).toList(),
            //     onChanged: ( value){
            //       setState(() {
            //         selectedStatus = value!;
            //       });
            //     }),

            DropdownButton<String>(
              value: _chosenValue,
              // icon:Icon(Icons.keyboard_arrow_down,) ,
              // iconEnabledColor: Colors.green,
              // style: TextStyle(color: Colors.black),
              items: <String>['Merchant','Option-1','Option-2','Option-3',].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(value: value, child: Text(value,),
                );
              }).toList(),
              onChanged: ( value) {
                setState(() {
                  _chosenValue = value!;
                });
              },
            ),



            SizedBox(height: 20,),
            TextField(
              controller: _titleEditingController,
              decoration: InputDecoration(
                  hintText: 'Enter Title',
                  labelText: 'Title',
                  border: OutlineInputBorder()
              ),
            ),
            SizedBox(height: 20,),
            TextField(
              controller: _descriptionEditingController,
              decoration: InputDecoration(
                  hintText: 'Enter Description',
                  labelText: 'Description',
                  border: OutlineInputBorder()
              ),
            ),
            SizedBox(height: 20,),
            Container(
              width: double.infinity,
              child: TextButton(
                onPressed: (){
                  validate();
                },child: Text(appBarTitle,style: TextStyle(color: Colors.blue),),),
            )

          ],
        ),
      ),
    );
  }

  validate(){
    toDoModel.title = _titleEditingController.text;
    toDoModel.description = _descriptionEditingController.text;
    toDoModel.status = selectedStatus;
    toDoModel.date = DateFormat.yMMMd().format(DateTime.now());

    DataBaseHelper dataBaseHelper = DataBaseHelper();

    if(toDoModel.id == null) {
      dataBaseHelper.insert(toDoModel);
    } else {
      dataBaseHelper.updateItem(toDoModel);
    }

    Navigator.pop(context,true);
  }

}