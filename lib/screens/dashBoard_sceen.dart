// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:devaseva_app/screens/locationList_screen.dart';
import 'package:devaseva_app/screens/logic_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  int _currentPosition = 2;

  var list = [
    Center(
        child: Text(
      "No Data Found",
      style: TextStyle(
          color: Colors.black87,
          fontSize: 16,
          fontWeight: FontWeight.w500,
          fontFamily: "Poppins"),
    )
    ),
    // Center(
    //     child: Text(
    //   "No Data Available",
    //   style: TextStyle(
    //       color: Colors.black87,
    //       fontSize: 16,
    //       fontWeight: FontWeight.w500,
    //       fontFamily: "Poppins"),
    // )),
    LogicScreen(),
    HomeScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: list[_currentPosition],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.greenAccent,
          unselectedItemColor: Colors.black26,
          currentIndex: _currentPosition,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.compass,
                size: 30,
              ),
              label: "Entdecken",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.add_circled,
                size: 30,
              ),
              label: "Tour erstellen",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.person_alt_circle,
                size: 30,
              ),
              label: "Profil",
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentPosition = index;
            });
          },
        ),
      ),
    );
  }
}
