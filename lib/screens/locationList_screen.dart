// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devaseva_app/models/locations_Model.dart';
import 'package:devaseva_app/screens/addDetails_screen.dart';
import 'package:devaseva_app/screens/locationView_screen.dart';
import 'package:devaseva_app/screens/login_screen.dart';
// import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<LocationsListResponse> locationsList = [];
  bool searchState = false;

  @override
  void initState() {
    // TODO: implement initState
    fetchRecords();
    super.initState();
  }

  fetchRecords() async {
    var records =
        await FirebaseFirestore.instance.collection('locations_list').get();
    mapRecords(records);
  }

  mapRecords(QuerySnapshot<Map<String, dynamic>> records) {
    var _list = records.docs
        .map(
          (locations) => LocationsListResponse(
            id: locations.id,
            title: locations['title'],
            subtitle: locations['subtitle'],
            cost: locations['cost'],
            time: locations['time'],
            distance: locations['distance'],
            image: locations['image'],
            subimage: locations['subimage'],
          ),
        )
        .toList();

    setState(() {
      locationsList = _list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.menu,
                  color: Colors.white,
                  size: 25,
                )),
            title: !searchState
                ? Text(
                    "Home",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                        fontSize: 20),
                  )
                : TextField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.search),
                      hintText: "Search...",
                      hintStyle: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontFamily: "Poppins"),
                    ),
                    onChanged: (text) {
                      // SearchMethod(text);
                    },
                  ),
            actions: [
              !searchState
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          searchState = !searchState;
                        });
                      },
                      icon: Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 25,
                      ))
                  : IconButton(
                      onPressed: () {
                        setState(() {
                          searchState = !searchState;
                        });
                      },
                      icon: Icon(
                        Icons.cancel,
                        color: Colors.white,
                        size: 25,
                      )),
              !searchState
                  ? IconButton(
                      onPressed: () {
                        showAlertDialog(context);
                      },
                      icon: Icon(
                        Icons.power_settings_new,
                        color: Colors.white,
                        size: 25,
                      ))
                  : Container(),

              SizedBox(width: 8,)
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddDetailsScreen()),
              );
            },
            backgroundColor: Colors.blue,
            child: const Icon(Icons.add),
          ),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: const TextSpan(
                    text: 'Firebase',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Poppins"),
                    children: [
                      TextSpan(
                        text: ' List',
                        style: TextStyle(
                            color: Colors.greenAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            fontFamily: "Poppins"),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: locationsList.length,
                    // itemCount: 2,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LocationViewScreen()),
                              );
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height: 65.0,
                                  width: 70.0,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: Colors.black26),
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            "${locationsList[index].image}"),
                                        fit: BoxFit.cover),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${locationsList[index].title}",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Poppins"),
                                    ),
                                    SizedBox(
                                      height: 05,
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          child: Icon(
                                            Icons.motorcycle,
                                            color: Colors.white,
                                            size: 16,
                                          ),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.greenAccent),
                                          padding: EdgeInsets.all(2.0),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "${locationsList[index].subtitle}",
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Poppins"),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          CupertinoIcons.list_dash,
                                          color: Colors.black87,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          width: 2,
                                        ),
                                        Text(
                                          "${locationsList[index].cost}",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Poppins"),
                                        ),
                                        SizedBox(
                                          width: 2,
                                        ),
                                        Icon(
                                          Icons.currency_rupee,
                                          color: Colors.black87,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          Icons.alarm,
                                          color: Colors.black87,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Text(
                                          "${locationsList[index].time}",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Poppins"),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Icon(
                                          CupertinoIcons.repeat,
                                          color: Colors.black87,
                                          size: 14,
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Text(
                                          "${locationsList[index].distance}",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: "Poppins"),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 75.0),
                            child: Divider(
                              color: Colors.black87,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      );
                    },
                  ),
                ),
                // Text("Hai"),
              ],
            ),
          )),
    );
  }

  void showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);

      },
    );
    Widget continueButton = TextButton(
      child: Text("LogOut",style: TextStyle(fontWeight: FontWeight.w700,color: Colors.black),),
      onPressed: () {

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
        );

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout",style: TextStyle(fontWeight: FontWeight.w700,color: Colors.black)),
      content: Text(
          "Are you sure want to logout from your app ?",),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  // void SearchMethod(String text) {
  //   DatabaseReference searchRef = FirebaseDatabase.instance.ref().child("Data");
  //   searchRef.once().then((DataSnapshot snapshot) {
  //     locationsList.clear();
  //     var keys = snapshot.value.keys;
  //     var values = snapshot.value;
  //     for
  //   });
  // }
}
