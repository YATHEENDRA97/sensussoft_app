// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class LocationViewScreen extends StatefulWidget {
  const LocationViewScreen({Key? key}) : super(key: key);

  @override
  _LocationViewScreenState createState() => _LocationViewScreenState();
}

class _LocationViewScreenState extends State<LocationViewScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Center(
          child: Text(
        "In Progress......",
        style: TextStyle(
            color: Colors.black87,
            fontSize: 16,
            fontWeight: FontWeight.w500,
            fontFamily: "Poppins"),
      )),
    ));
  }
}
