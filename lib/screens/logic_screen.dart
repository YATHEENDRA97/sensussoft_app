// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class LogicScreen extends StatefulWidget {
  const LogicScreen({Key? key}) : super(key: key);

  @override
  State<LogicScreen> createState() => _LogicScreenState();
}

class _LogicScreenState extends State<LogicScreen> {
  logic() {
    List<CalenderItem> calenderDateList = [];
    List<MeditationList> meditationList = [
      MeditationList(
          calendarDates: ["22/01/2022", '23/02/2022'], playActions: [])
    ];

    if (calenderDateList.isEmpty) {
      for (var m in meditationList) {
        for (var cal in m.calendarDates!) {
          calenderDateList
              .add(CalenderItem(calDateTime: cal, calenderData: []));
        }
      }
    } else {
      for (var m in meditationList) {
        for (var calDateList in calenderDateList) {
          bool exits = m.calendarDates!.contains(calDateList.calDateTime);
          print("EXITS  : $exits");

          if (!exits) {
            int index = m.calendarDates!
                .indexWhere((element) => element == calDateList.calDateTime);
            calenderDateList.add(CalenderItem(
                calDateTime: m.calendarDates![index], calenderData: []));
          }
        }
      }
    }
    print("Length:::${calenderDateList.length}");
    for (var element in calenderDateList) {
      print("Dates:::${element.calDateTime}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blueAccent,
          onPressed: () {
            logic();
          },
          child: Text(
            "Click",
            style: TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.w500,
                fontFamily: "Poppins"),
          )),
      body: Center(
        child: Text(
          "Data",
          style: TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w500,
              fontFamily: "Poppins"),
        ),
      ),
    );
  }
}

class Constants {
  static const String type_meditation = "TYPE";
}

class MeditationList {
  MeditationList({
    this.calendarDates,
    this.playActions,
  });

  List<String>? calendarDates;
  List<dynamic>? playActions;

  MeditationList copyWith({
    List<String>? calendarDates,
    List<dynamic>? playActions,
  }) =>
      MeditationList(
        calendarDates: calendarDates ?? this.calendarDates,
        playActions: playActions ?? this.playActions,
      );
}

class CalenderItem {
  String? calDateTime;
  List<CalenderData>? calenderData;

  CalenderItem({required this.calDateTime, required this.calenderData});
}

class CalenderData {
  MeditationList? meditations;
  String? calenderType;
  CalenderData({required this.meditations, required this.calenderType});
}
